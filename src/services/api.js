import axios from 'axios';

const api = axios.create({
  'baseURL': 'http://177.74.67.115:5000/domains',
});

export default api;