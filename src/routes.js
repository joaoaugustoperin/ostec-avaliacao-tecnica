import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';

import Create from './pages/Create';
import Edit from './pages/Edit';
import List from './pages/List';

export default function Routes() {
  return (
    <BrowserRouter>
      <Switch>
        <Route exact path="/" component={List} />
        <Route exact path="/edit" component={Edit} />
        <Route exact path="/create" component={Create} />
      </Switch>
    </BrowserRouter>
  );
}