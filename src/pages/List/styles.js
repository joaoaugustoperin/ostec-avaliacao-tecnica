import styled from 'styled-components';

export const Container = styled.div`
  @import url('https://fonts.googleapis.com/css2?family=Roboto:wght@300&display=swap');

  width: 100vw;
  min-width: 100vw;
  height: 100vh;
  min-height: 100vh;

  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;

  font-family: 'Roboto';
  font-weight: lighter;

  img {
    height: 20%;
    margin-bottom: 5%;
  }
`;

export const ConstraintBox = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  flex-direction: row;
  width: 70%;
  font-size: 70%;
`;
