import React, { useState, useEffect } from 'react';
import { DataGrid } from '@material-ui/data-grid';
import { Button } from '@material-ui/core';
import OstecLogo from '../../assets/ostec_logo.png';
import api from '../../services/api';

import { Container, ConstraintBox } from './styles';

function List({ history }) {
  const DELETE_FIELD = "delete";
  const EDIT_FIELD = "edit";
  const [rows, setRows] = useState([]);

  const fetchData = async () => {
    api.get('', { "Content-Type": "application/json" }).then(res => {
      // Tratativa pois ao deletar, o id é persistido. Filtrar este comportamento:
      const filtered = res.data.filter((value) => ((value.domain && value.description)));

      // Adiciona ícones e edição e deleção para todas as linhas filtradas:
      filtered.map(value => {
        value['edit'] = '🗒';
        value['delete'] = '🗑';
      });

      // Após filtrado, adicionado ao estado de linhas da data-grid.
      setRows(filtered);
    });
  }

  useEffect(() => {
    fetchData();
  }, []);

  const columns = [
    { field: 'domain', headerName: 'Domínio', width: 150 },
    { field: 'description', headerName: 'Descrição', width: 150 },
    { field: EDIT_FIELD, headerName: 'Editar', width: 150 },
    { field: DELETE_FIELD, headerName: 'Excluir', width: 150 },
  ];

  const handleCellClick = async e => {
    if (e.field === DELETE_FIELD) {
      api.delete(`/${e.row.id}`);
      fetchData();
      document.location.reload();
    } else if (e.field === EDIT_FIELD) {
      history.push('/edit', { domainId: e.row.id });
    }
  }

  return (
    <Container>
      <img src={OstecLogo} alt="OSTEC DOMAIN APP" style={{ height: 100 }} />
      <ConstraintBox>
        <h1>Seja Bem vindo ao Data-Domain-Grid da OSTEC!</h1>
        <Button variant="contained" color="primary" onClick={() => history.push('/create')} >
          Criar Registro
        </Button>
      </ConstraintBox>
      <br />
      <div style={{ height: 300, width: '100%' }}>
        {rows.length > 0 &&
          <DataGrid
            rows={rows}
            columns={columns}
            onCellClick={handleCellClick}
          />}
        {(!rows || rows.length < 0) && <h1>Procurando Dados...</h1>}
      </div>
    </Container>
  );
}

export default List;