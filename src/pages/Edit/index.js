import React, { useEffect, useState } from 'react';
import TextField from '@material-ui/core/TextField'
import { Button } from '@material-ui/core';
import OstecLogo from '../../assets/ostec_logo.png';
import api from '../../services/api';

import { Container, ButtonContainer } from './styles';

function Edit({ history, location }) {
  const domainId = location.state.domainId;
  const [domainEntity, setDomainEntity] = useState({});
  const [domain, setDomain] = useState("");
  const [description, setDescription] = useState("");

  const fetchData = async () => {
    api.get(`/${domainId}`).then(async res => {
      await setDomainEntity(res.data);
    });
  }

  const handleUpdate = async () => {
    api.put(`/${domainId}`, { domain, description }).then(res => {
      if (res && (res.status == 200 || res.status == 201)) {
        history.goBack();
      } else {
        alert("Algum erro aconteceu ao tentar atualizar registro!");
      }
    });
  }

  useEffect(() => {
    fetchData();
  }, []);

  useEffect(() => {
    setDomain(domainEntity.domain);
    setDescription(domainEntity.description);
  }, [domainEntity]);

  return (
    <>
      <Container>
        <img src={OstecLogo} alt="OSTEC DOMAIN APP" style={{ height: 100 }} />

        <TextField id="domain" label="Domínio" variant="outlined" onChange={(e) => setDomain(e.target.value)} value={domain} />
        <TextField id="description" label="Descrição" variant="outlined" style={{ marginTop: "2%" }} onChange={(e) => setDescription(e.target.value)} value={description} />

        <ButtonContainer>
          <Button variant="contained" color="secondary" onClick={history.goBack} >
            Voltar
          </Button>

          <Button variant="contained" color="primary" onClick={handleUpdate} >
            Atualizar Registro
          </Button>
        </ButtonContainer>
      </Container>
    </>
  );
}

export default Edit;