import React, { useState } from 'react';
import TextField from '@material-ui/core/TextField'
import { Button } from '@material-ui/core';
import OstecLogo from '../../assets/ostec_logo.png';
import api from '../../services/api';
import { Container, ButtonContainer } from './styles';

function Create({ history }) {
  const [domain, setDomain] = useState("");
  const [description, setDescription] = useState("");

  const handleCreate = async () => {
    api.post('', { domain, description }).then(res => {
      if (res && (res.status == 200 || res.status == 201)) {
        history.goBack();
      } else {
        alert("Algum erro aconteceu ao tentar registrar!");
      }
    });
  }

  return (
    <>
      <Container>
        <img src={OstecLogo} alt="OSTEC DOMAIN APP" style={{ height: 100 }} />

        <TextField id="domain" label="Domínio" variant="outlined" onChange={(e) => setDomain(e.target.value)} />
        <TextField id="description" label="Descrição" variant="outlined" style={{ marginTop: "2%" }} onChange={(e) => setDescription(e.target.value)} />

        <ButtonContainer>
          <Button variant="contained" color="secondary" onClick={history.goBack} >
            Voltar
          </Button>

          <Button variant="contained" color="primary" onClick={handleCreate} >
            Criar Registro
          </Button>
        </ButtonContainer>
      </Container>
    </>
  );
}

export default Create;