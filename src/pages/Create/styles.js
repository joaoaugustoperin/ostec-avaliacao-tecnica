import styled from 'styled-components';

export const Container = styled.div`
  width: 100vw;
  min-width: 100vw;
  height: 100vh;
  min-height: 100vh;

  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;

  img {
    height: 20%;
    margin-bottom: 5%;
  }
`;

export const ButtonContainer = styled.div`
  margin-top: 3%;
  width: 30%;

  display: flex;
  align-items: center;
  justify-content: space-between;
  flex-direction: row;
`;
